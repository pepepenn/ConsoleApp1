# RPG Heroes
Author: Pepe Penninkhof

RPG Heroes is an application that allows for the creation of RPG hero classes that have "classes" such as mage and warrior. The heroes can be interacted with in ways such as levelling up and keeping track of inventory. Furthermore, the application allows for the creation of items such as pieces of armor and weapons that have can be given unique properties to improve the attributes of the hero.



Foobar is a Python library for dealing with word pluralization.



## Usage

```csharp
//Makes a hero with the class warrior
Warrior thewarrior = new Warrior() { Name = "Kaladin Stormblessed" };
// Makes an item, a piece of armor
Armor masterbody = new Armor() { Name = "Common Plate Chest", RequiredLevel = 1, Slot = Slot.Body, armortype = Armor.ArmorType.Plate, ArmorAttributes = new BaseAttribute() {Dexterity = 0, Intelligence = 0, Strength = 1} };
// Makes a weapon
Weapon mastersword = new Weapon() { Name = "Common Axe", RequiredLevel = 1, Slot = Slot.Weapon, weapontype = Weapon.WeaponType.Axes, weaponDamage = 2};


```

## Contributing

Pull requests are welcome. For major changes, please open an issue first
to discuss what you would like to change.

Please make sure to update tests as appropriate.
